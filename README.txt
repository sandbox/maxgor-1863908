DESCRIPTION
------------------------
This is a small module for Commerce module that allows you to export
orders into CSV file.


INSTALLATION
-----------------------
Copy the module's directory to your modules directory and activate the
module.


Usage
-----------------------
To export into CSV you need to go to Store -> Export Order.
Select a range of the created order and click Eport button.
