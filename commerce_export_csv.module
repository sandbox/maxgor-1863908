<?php

/**
 * @file
 * The commerce_export_csv module
 *
 * Allows you to export orders of Commerce module into CSV file
 */

/**
 * Implements hook_menu().
 */
function commerce_export_csv_menu() {
  $items['admin/commerce/export_orders'] = array(
    'title' => 'Export Orders',
    'description' => 'Export Orders by _date',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_export_csv_config'),
    'access arguments' => array('configure order settings'),
  );

  return $items;
}

/**
 *  Order export configuration form.
 */
function commerce_export_csv_config() {
  $form['page_title'] = array(
    '#markup' => t('Receive CSV file with orders within a date range'),
  );
  $form['from_date'] = array(
    '#type' => 'date',
    '#title' => t('From _date'),
    '#description' => t('Select start date'),
    '#default_value' => array(
      'month' => format_date(time(), 'custom', 'n'),
      'day' => format_date(time(), 'custom', 'j'),
      'year' => format_date(time(), 'custom', 'Y'),
    ),
    '#required' => TRUE,
  );
  $form['to_date'] = array(
    '#type' => 'date',
    '#title' => t('To _date'),
    '#description' => t('Select final date'),
    '#default_value' => array(
      'month' => format_date(time(), 'custom', 'n'),
      'day' => format_date(time(), 'custom', 'j'),
      'year' => format_date(time(), 'custom', 'Y'),
    ),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );
  return $form;
}

/**
 * Callback submit function.
 */
function commerce_export_csv_config_submit(&$form, &$form_state) {
  global $user;
  $timezone = $user->timezone;

  $from_date = $form_state['values']['from_date'];
  $to_date = $form_state['values']['to_date'];

  $from_date = "{$from_date['year']}-{$from_date['month']}-{$from_date['day']}";
  $to_date = "{$to_date['year']}-{$to_date['month']}-{$to_date['day']}";

  $from_date_unix = format_date(strtotime($from_date), 'custom', 'd.m.Y', $timezone);
  $from_date_unix = strtotime($from_date_unix);

  $to_date_unix = format_date(strtotime($to_date), 'custom', 'd.m.Y', $timezone);
  $to_date_unix = strtotime($to_date_unix);

  $data = db_select('commerce_order', 'co');
  $data->fields('co', array('order_id'));
  $data->condition('co.created', array($from_date_unix, $to_date_unix), 'BETWEEN');
  $data->orderBy('co.created', 'DESC');

  $orders = $data->execute()->fetchAll();

  if (empty($orders)) {
    drupal_set_message(t('Orders are not found!'), 'error');
    drupal_goto('admin/commerce/export_orders');
  }
  $order_ids = array();
  foreach ($orders as $key => $value) {
    $order_ids[] = $value->order_id;
  }

  $orders = commerce_order_load_multiple($order_ids);
  commerce_export_csv_save_to_csv($orders);
}

/**
 * Function for exporting orders into CSV.
 */
function commerce_export_csv_save_to_csv($orders, $filename = 'orders.csv') {
  drupal_add_http_header('Content-Type', 'text/csv; utf-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename=' . $filename);

  $header = array();
  $header[] = '"Order ID"';
  $header[] = '"_date"';
  $header[] = '"User name"';
  $header[] = '"Product title"';
  $header[] = '"Unit Price"';
  $header[] = '"Quantity"';
  $header[] = '"Total"';
  $header[] = '"Order status"';

  print implode(',', $header) . "\r\n";

  foreach ($orders as $val) {
    foreach ($val->commerce_line_items[LANGUAGE_NONE] as $key => $value) {
      $item = commerce_line_item_load($value['line_item_id']);
      $product = commerce_product_load($item->commerce_product[LANGUAGE_NONE][0]['product_id']);
      $user = user_load($val->uid);
      $values = array();
      $values[] = '"' . $val->order_id . '"';
      $values[] = '"' . format_date($val->created, 'short') . '"';
      $values[] = '"' . $user->name . '"';
      $values[] = '"' . $product->title . '"';
      $values[] = '"' . commerce_currency_format($item->commerce_unit_price[LANGUAGE_NONE][0]['amount'], commerce_default_currency()) . '"';
      $values[] = '"' . (int) $item->quantity . '"';
      $values[] = '"' . commerce_currency_format($item->commerce_total[LANGUAGE_NONE][0]['amount'], commerce_default_currency()) . '"';
      $values[] = '"' . $val->status . '"';

      print implode(',', $values) . "\r\n";

    }
  }

  drupal_exit();
}
